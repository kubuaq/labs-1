﻿using Lab1.Contract;
using Lab1.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(IMałpa);

        public static Type ISub1 = typeof(IPawian);
        public static Type Impl1 = typeof(Małpa);

        public static Type ISub2 = typeof(IGoryl);
        public static Type Impl2 = typeof(Goryl);


        public static string baseMethod = "metodaint";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "metoda2";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "metoda1";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "Kolekcja";
        public static string collectionConsumerMethod = "Testing";

        #endregion

        #region P3

        public static Type IOther = typeof(IOlbrzym);
        public static Type Impl3 = typeof(Pawian);

        public static string otherCommonMethod = "metodaint";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
